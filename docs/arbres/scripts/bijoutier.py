from binarytree import Node

# Créer un arbre
arbre = Node(7)
arbre.left = Node(3)
arbre.left.left = Node(1)
arbre.right = Node(8)
arbre.left.right = Node(6)

print(arbre)
