def parcours_prefixe_liste(arbre):
    if arbre is None : 
        return []
    return [arbre.valeur] + parcours_prefixe_liste(arbre.gauche) + parcours_prefixe_liste(arbre.droit)
    