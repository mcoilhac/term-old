from queue import Queue

def parcours_BFS(arbre):
    file = Queue()
    file.put(arbre)
    solution = []
    while not file.empty():
        a = file.get()
        if a is not None :
            solution.append(a.data)
            file.put(a.left)
            file.put(a.right)
    return solution
