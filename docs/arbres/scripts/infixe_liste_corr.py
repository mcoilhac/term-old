def parcours_infixe_liste(arbre):
    if arbre is None : 
        return []
    return parcours_infixe_liste(arbre.gauche) + [arbre.valeur] + parcours_infixe_liste(arbre.droit) 
