class Personnage_5:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(self):
        return self.vie

    def perd_une_vie(self):
        self.vie = self.vie - 1

    def boire_potion(self):
        self.vie = self.vie + 1

def simul(n, a):
    mon_personnage = Personnage_5(n, a)
    mon_personnage.boire_potion()
    return mon_personnage.donne_etat()



