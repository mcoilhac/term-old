from random import randint

class Personnage_7:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(self):
        return self.vie

    def perd_vies(self, vies_perdues):
        perdues = randint(1, vies_perdues)
        self.vie = self.vie - perdues

    def boire_potion(self):
        self.vie = self.vie + 1

monstre = Personnage_7(100, 130)
for i in range(3):
    monstre.perd_vies(10)
print(monstre.donne_etat())



