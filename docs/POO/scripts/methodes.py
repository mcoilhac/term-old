class Personnage_4:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(...):
        ...

    def perd_une_vie(...):
        ...

gollum = ...
etat_1 = ...
print(etat_1)
...  # gollum perd une vie
etat_2 = ...
print(etat_2)


