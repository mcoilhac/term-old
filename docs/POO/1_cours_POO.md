---
author: Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title: Vocabulaire de la POO
---

## I. Introduction

!!! info "Le paradigme objet"

    Il existe différentes manières de voir la programmation, on parle de différents paradigmes de programmation. L'un d'eux est le paradigme objet. Lorsque l'on programme en utilisant ce paradigme on parle de programmation objet ou de programmation orientée objet (abrégé POO, ou OOP en anglais pour « Object-oriented programming ».
	Nous nous limiterons cette année à une introduction de la programmation objet.



???+ question "Une approche de la POO"

	<div class="centre" markdown="span">
	<iframe 
	src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/intro_POO_Fluoresciences_2023.ipynb"
	width="900" height="700" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


## II. Vocabulaire de la programmation objet

??? note "Les objets"

	La programmation objet consiste à regrouper données et traitements dans une même structure appelée objet. Elle possède l'avantage de localiser en un même endroit toute l'implémentation d'une structure de données abstraite.

!!! example "Prenons une voiture comme exemple"

    Une voiture peut être considérer comme un objet.

	* On peut lui associer des informations comme sa couleur, sa marque et sa catégorie : il s'agit des **attributs** de notre objet.

	* On peut également définir des mécanismes concernant cet objet comme démarrer, accélérer, freiner, klaxonner : il s'agit des **méthodes** qui vont s'appliquer sur notre objet.

!!! info "Les objets"

	Concrètement, un objet est une structure de données abstraite regroupant :

	* des données associées à cet l'objet que l'on appelle des **attributs**.
	* des fonctions (ou procédures) s'appliquant sur l'objet que l'on appelle des **méthodes**.

!!! example "Exemple de **classe**"

	Reprenons l'exemple de la voiture

	Ses attributs ( couleur, marque, ...) et méthodes ( démarrer, accélerer, ...) sont réunis dans ce qu'on appelle une  classe qui est donc un modèle (moule) décrivant un objet, ici la voiture. On va donc définir une classe `Voiture` qui va être le moule (modèle) pour la fabrication de tous les objets `voitures`. On peut la schématiser ainsi :

	```mermaid
		classDiagram
    	class Voiture{
      		String Couleur
      		String Marque
      		String Catégorie
      		Démarrer()
      		Accélérer()
      		Freiner()
      		Klaxonner()
    	}
    ```


	Les objets sont ensuite créés à partir de ce moule. On peut fabriquer deux objets `clio` et `c3`, qui sont deux instances de la classe `Voiture` en écrivant simplement les deux instructions suivantes :

	```python
	clio = Voiture()
	c3 = Voiture()
	```
	
!!! info "Les instances"

	On va donc pouvoir créer facilement des objets `Voiture` grâce à cette classe (moule).

	Il suffit pour les construire d'utiliser le nom de la classe (qui est aussi le nom du constructeur d'objets de cette classe). Chaque objet ainsi créé est une **instance** de la classe.

!!! warning "Remarque"

    * Les méthodes définies dans une classe ne sont applicables **que sur les objets instances de la classe**. 
    * Inversement, un objet instance d’une classe ne peut se voir appliquer **que les méthodes de cette classe**.

!!! info "Accès aux attributs et aux méthodes"

	Pour accéder aux attributs et aux méthodes d'une classe on utilise la **notation pointée**.

	Dans notre exemple :

	* `clio.marque` donne la marque de l'objet clio, c'est à dire Renault
	* `clio.klaxonner()` va faire klaxonner notre voiture (virtuelle). Nous reviendrons sur cela un peu plus loin.


## III. Classes et objets en Python

??? note "En Python, tout est objet !"

    Vous ne le saviez sans doute pas, mais les objets vous connaissez déjà (et oui !)

???+ question "Les listes en Python"

	Les listes sont un type abstrait `list` dans python; et vous utilisez la notation pointée pour ajouter un élément.

    Recopier une par une les lignes suivantes, et les exécuter une par une.

    ```pycon
    >>> ma_liste = [2, 3, 5]
    >>> type(ma_liste)
    >>> ma_liste.append(8)
    >>> ma_liste
    ```

	{{ terminal() }}


???+ question "Les types en Python"

	L'affichage montre que tous les types en Python sont des classes. Les entiers sont des objets de la classe `int`, les flottants sont des objets de la classe `float`, etc. Pour créer un entier ou une liste il suffit de les construire en utilisant le nom de leurs classes respectives

    Recopier une par une les lignes suivantes, et les exécuter une par une.

    ```pycon
    >>> type(int)
    >>> type(float)
    >>> entier = int()
    >>> type(entier)
    >>> ma_liste = list()
    >>> type(ma_liste) 
    ```

    {{ terminal() }}


???+ question "Interface d'une classe"

	En définissant une classe on définit un type abstrait de données. Comme tout type abstrait, une classe possède donc une interface qui décrit l'ensemble des méthodes auxquelles on a accès pour manipuler les objets de cette classe.

	On peut utiliser la fonction `dir` pour lister tous les attributs et méthodes d'un objet. 

	Taper `dir(list)` dans la console python pour visualiser les différentes méthodes et attributs de `list`.

    {{ terminal() }}



!!! info "Les méthodes du type prédéfini `list`"

	On retrouve ici les méthodes applicables sur les objets du type prédéfini `list`.

	Son interface est disponible dans la [documentation officielle](https://docs.python.org/fr/3/tutorial/datastructures.html#more-on-lists){:target="_blank" }

	Vous noterez que l'interface ne précise pas la façon dont les méthodes sont implémentées mais juste la façon de les utiliser, ce qui suffit largement généralement.

	On constate aussi qu'il y a de nombreuses méthodes dont le nom est encadré d'un double underscore `__`.  
	Ce sont des **méthodes spéciales** . Nous reviendrons sur quelques-unes d'entre elles un peu plus tard.


## IV. Attributs, paramètres et paramètres par défaut

???+ question "Attributs, paramètres et paramètres par défaut"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `parametres_attributs_2022_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/parametres_attributs_2022_sujet.ipynb)

    ⏳ La correction viendra bientôt ... 

<!--- Correction
🌐 Fichier `parametres_attributs_2022_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/parametres_attributs_2022_corr.ipynb)
-->


## V. Bilan

!!! info "Le paradigme objet "

	Le paradigme objet est une autre façon de voir la programmation qui consiste à utiliser une structure de données appelée objet qui réunit des données et des fonctionnalités. Les données sont appelées **attributs** et les fonctionnalités sont appelées **méthodes**.
	
!!! info "Une classe "

	Une classe permet de définir un ensemble d’objets qui ont des caractéristiques communes. C’est un moule permettant de créer une infinité d’objets différents dans leurs valeurs mais similaires dans leur structure.

!!! info "Le mot clé `class` et les instances de classes"

	En Python, on utilise le mot clé `class` pour définir une classe qui devient alors un nouveau type abstrait de données. 

	On peut alors créer de nouveaux objets en appelant le constructeur qui porte le nom de la classe. Les objets ainsi créés s'appellent des **instances** de la classe.

!!! info "La méthode `__init__` "

	 En Python, la méthode `__init__` est le constructeur de la classe. Elle est utilisée à la création des objets de la classe et initialise les valeurs des attributs de l’objet.

!!! info "La notation pointée"

	Les attributs et méthodes d'une instance de classe sont accessibles en utilisant la notation pointée : 

	* `objet.attribut`
	* `objet.methode(arguments)`