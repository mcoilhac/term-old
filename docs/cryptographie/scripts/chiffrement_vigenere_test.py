# Test
assert chiffre_vigenere('incroyable','nsi') == 'vfkeggnttr'
assert chiffre_vigenere('incroyable','a') == 'incroyable'

# Autres tests
assert chiffre_vigenere('hello','nsi') == "uwtyg"

