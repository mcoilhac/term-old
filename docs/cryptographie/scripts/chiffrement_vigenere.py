def chiffre_vigenere(texte: str, clef: str ) -> str :
    """
    Préconditions :
    - texte est le texte à chiffrer (type str)
    - clef est la clef (type str)
    Postconditions :
    la fonction renvoie le texte chiffré : type str.
    Par exemple chiffre_vigenere('incroyable','nsi') renvoie 'vfkeggnttr'
    """

    ...

nombre_lettre = cree_nombre_lettre() # nombre_lettre = {0: 'a', 1: 'b', 2: 'c',
                                     # 3: 'd', etc...}
lettre_nombre = cree_lettre_nombre() # lettre_nombre = {'a': 0, 'b': 1, 'c': 2,
                                     # 'd': 3, etc...}

# Test
assert chiffre_vigenere('incroyable','nsi') == 'vfkeggnttr'
assert chiffre_vigenere('incroyable','a') == 'incroyable'