def profondeur_opt(graphe, depart, vu = None, parcours = None):
    if vu is None:
        vu = {sommet : False for sommet in graphe}
    if parcours  is None:
         parcours = []
    vu[depart] = True
    parcours.append(depart)
    for voisin in graphe[depart]:
        if not vu[voisin]:
            profondeur_opt(graphe, voisin, vu, parcours)
    return vu, parcours

