def profondeur_opt(graphe, depart, vu = None, parcours = None):
    if vu is None:
        vu = {sommet : False for sommet in graphe}
    if parcours  is None:
         parcours = []
    vu[depart] = True
    parcours.append(...)
    for voisin in graphe[depart]:
        if ...:
            ...
    return vu, parcours

# Tests
print(profondeur_opt({"A": ["B", "D", "E"],"B": ["A", "C"], "C": ["B", "D"],
"D": ["A", "C", "E"], "E": ["A", "D", "F", "G"], "F": ["E", "G"], "G": ["E", "F", "H"], 
"H": ["G"]}, "A"))
