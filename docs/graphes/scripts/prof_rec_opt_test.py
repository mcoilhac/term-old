# Tests
assert profondeur_opt({"A": ["B", "D", "E"],"B": ["A", "C"], "C": ["B", "D"],
"D": ["A", "C", "E"], "E": ["A", "D", "F", "G"], "F": ["E", "G"], "G": ["E", "F", "H"], 
"H": ["G"]}, "A") ==  ({'A': True, 'B': True, 'C': True, 'D': True, 'E': True, 'F': True, 'G': True, 'H': True}, 
['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'])

# Autres tests
graphe_6 = {"A":["B", "C"], "B":["A"], "C":["A", "D", "E"], "D": ["C", "E"], "E":["C", "D"]}
assert profondeur_opt(graphe_6, "A") == ({'A': True, 'B': True, 'C': True, 'D': True, 'E': True}, 
['A', 'B', 'C', 'D', 'E'])



