???+ note dépliée

    On aurait pu également envisager une boucle `for` avec sortie anticipée si le plan n'est pas un cycle :

    ```python
    def est_cyclique(plan):
        """
        Prend en paramètre un dictionnaire plan.
        Renvoie True si le plan d'envoi de messages est cyclique
        et False sinon.
        """
        personnes = [key for key in plan]
        premiere = personnes[0]
        personne = premiere
        effectif = len(plan)
        for i in range(effectif - 1):
            personne = plan[personne]
            if personne == premiere:
                return False
        return True
    ```

!!! info "Graphe"

    Cet exercice ressemble à un exercice de graphe, mais on est dans un cas particulier qui simplifie le parcours.
