---
author: Nicolas Revéret
title: Recherche dichotomique itérative (2)
tags:
  - 5-dichotomie
  - important
---

# Recherche dichotomique itérative

On considère dans cet exercice des tableaux non vides contenant des nombres entiers, tous distincts, triés dans l'ordre croissant.

On cherche à déterminer l'indice d'une valeur `#!py cible` dans ce tableau à l'aide d'une **recherche dichotomique** dans sa version itérative.

Écrire la fonction `#!py indice` qui prend en paramètres le tableau de nombres `#!py tableau` et la valeur cherchée `#!py cible`.

Si la `#!py cible` est dans le tableau, la fonction renverra son indice. Dans le cas contraire, la fonction renverra `None`.


???+ danger "Attention"

    Les tableaux des tests secrets peuvent être très grands. Une recherche linéaire naïve prendrait trop de temps lors de l'exécution.

    Les tests secrets limitent le nombre de lectures dans le tableau à 100. Si votre code accède à plus de 100 valeurs dans le tableau, une erreur sera levée.


???+ example "Exemples"

    ```pycon
    >>> tableau = [23, 28, 29, 35, 37]
    >>> indice(tableau, 23)
    0
    >>> indice(tableau, 29)
    2
    >>> indice(tableau, 37)
    4
    >>> indice(tableau, 10)
    None
    >>> indice(tableau, 100)
    None
    ```

???+ question

    Compléter le script ci-dessous :
    
    {{ IDE('exo') }}
    


