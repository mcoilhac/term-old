# Tests
arbre = ( ( (None, 1, None), 2, (None, 3, None) ), 4, ( (None, 5, None), 6, (None, 7, None) ) )
assert parcours_largeur(arbre) == [4, 2, 6, 1, 3, 5, 7]

# Autres tests
 arbre_2 = ( ( (None, 11, None), 12, (None, 13, None) ), 4, ( (None, 15, None), 16, (None, 17, None) ) )
parcours_largeur(arbre_2) == [4, 12, 16, 11, 13, 15, 17]