---
author: Gilles Lassus puis Mireille Coilhac
title: Crible d'Eratosthène
tags:
  - liste/tableau
  - Difficulté **
  - important
---

Un nombre premier est un nombre entier naturel qui admet exactement deux diviseurs distincts
entiers et positifs : 1 et lui-même. 

Le crible d’Ératosthène permet de déterminer les nombres premiers plus petit qu’un certain
nombre `n` fixé. 

On considère pour cela un tableau `tab` de `n`booléens, initialement tous égaux à `True`, sauf
`tab[0]` et `tab[1]` qui valent `False`, 0 et 1 n’étant pas des nombres premiers.  

On parcourt alors ce tableau de gauche à droite.  

Pour chaque indice `i` :

- si `tab[i]` vaut `True` : le nombre `i` est premier et on donne la valeur `False` à toutes les
cases du tableau dont l’indice est un multiple de `i`, à partir de `2*i` (c’est-à-dire `2*i`, `3*i` ...).

- si `tab[i]` vaut `False` : le nombre `i` n’est pas premier et on n’effectue aucun
changement sur le tableau. 

On dispose de la fonction `crible`, incomplète et donnée ci-dessous, prenant en paramètre un
entier `n` strictement positif et renvoyant un tableau contenant tous les nombres premiers plus
petits que `n`.


<p><a href="https://commons.wikimedia.org/wiki/File:Sieve_of_Eratosthenes_animation.gif#/media/File:Sieve_of_Eratosthenes_animation.gif"><img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/Sieve_of_Eratosthenes_animation.gif" alt="Sieve of Eratosthenes animation.gif"></a><br>

By <a href="https://en.wikipedia.org/wiki/de:User:SKopp" class="extiw" title="w:de:User:SKopp"&gt;SKopp&lt;/a&gt; at &lt;a href="https://en.wikipedia.org/wiki/de:" class="extiw" title="w:de:"&gt;German Wikipedia&lt;/a&gt; - &lt;span class="int-own-work" lang="en"&gt;Own work&lt;/span&gt;, Original image at &lt;a href="//commons.wikimedia.org/wiki/File:Animation_Sieve_of_Eratosth.gif" title="File:Animation Sieve of Eratosth.gif"&gt;Image:Animation_Sieve_of_Eratosth.gif&lt;/a&gt;, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=2810935">Link</a></p>

Auteur  SKopp sur Wikipedia allemand


???+ question "Compléter la fonction `crible`" 

    {{ IDE('exo') }}





