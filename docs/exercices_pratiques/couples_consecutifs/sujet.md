---
author: Gilles Lassus puis Mireille Coilhac
title: Couples consécutifs
tags:
  - tuple
  - Difficulté **
  - important
---

Écrire une fonction `couples_consecutifs` qui prend en paramètre un tableau de
nombres entiers `tab` non vide (type `list`), et qui renvoie la liste Python (éventuellement vide) des couples d'entiers consécutifs successifs qu'il peut y avoir dans `tab`.

!!! example "Exemples"

    ```pycon
    >>> couples_consecutifs([1, 4, 3, 5])
    []
    >>> couples_consecutifs([1, 4, 5, 3])
    [(4, 5)]
    >>> couples_consecutifs([1, 1, 2, 4])
    [(1, 2)]
    >>> couples_consecutifs([7, 1, 2, 5, 3, 4])
    [(1, 2), (3, 4)]
    >>> couples_consecutifs([5, 1, 2, 3, 8, -5, -4, 7])
    [(1, 2), (2, 3), (-5, -4)]
    ```

???+ question "Compléter le code ci-dessous"

    {{ IDE('exo') }}