from math import sqrt     # import de la fonction racine carrée

def distance(point_1, point_2):
    """ Calcule et renvoie la distance entre deux points. """
    return sqrt((point_1[0] - point_2[0])**2 + (point_1[1] - point_2[1])**2)

def proche_voisin(tab, depart):
    """ Renvoie le point du tableau tab se trouvant à la plus courte distance du point depart."""
    point = tab[0]
    min_dist = distance(point, depart)
    for i in range (1, len(tab)):
        if distance(tab[i], depart) < min_dist:
            point = tab[i]
            min_dist = distance(tab[i], depart)
    return point



