# Tests
assert distance((1, 0), (5, 3)) == 5
assert distance((1, -4), (6, 8)) == 13
assert proche_voisin([(7, 9), (2, 5), (5, 2)], (0, 0)) == (2, 5)
assert proche_voisin([(7, 9), (2, 5), (5, 2)], (5, 2)) == (5, 2)

# Autre tests
assert proche_voisin([(7, 9), (2, 5), (5, 2), (3, 4)], (0, 0)) == (3, 4)
assert proche_voisin([(i, 1) for i in range(-50, 50)], (5, 3)) == (5, 1)


