class Noeud:
    def __init__(self, etiquette, gauche, droit):
        self.v = etiquette
        self.gauche = gauche
        self.droit = droit


def taille(a):
    ...

def hauteur(a):
    ...


# Tests

a = Noeud(1, Noeud(4, None, None), Noeud(0, None, Noeud(7, None, None)))
assert hauteur(a) == 2
assert taille(a) ==  4
assert hauteur(None) == -1
assert taille(None) == 0
assert hauteur(Noeud(1, None, None)) == 0
assert taille(Noeud(1, None, None)) == 1
