# Tests

assert ajoute(1, 4, [7, 8, 9]) == [7, 4, 8, 9]
assert ajoute(3, 4, [7, 8, 9]) == [7, 8, 9, 4]
assert ajoute(0, 4, [7, 8, 9]) == [4, 7, 8, 9]

# Autres tests

assert ajoute(0,10,[]) == [10]
assert ajoute(2, 5, [7, 8, 9]) == [7, 8, 5, 9]
