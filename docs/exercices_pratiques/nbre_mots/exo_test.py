# Tests
assert nombre_de_mots('Cet exercice est simple.') == 4
assert nombre_de_mots('Le point d exclamation est séparé !') == 6
assert nombre_de_mots('Combien de mots y a t il dans cette phrase ?') == 10
assert nombre_de_mots('Fin.') == 1

# Autres tests
assert nombre_de_mots('') == 0
assert nombre_de_mots('Hello !') == 1
assert nombre_de_mots('Hello.') == 1
