# Tests
resultats_2 = {'Dupont': {
                        'DS1': [15.5, 4],
                        'DM1': [14.5, 1],
                        'DS2': [13, 4],
                        'PROJET1': [16, 3],
                        'DS3': [14, 4]
                    },
            'Durand': {
                        'DS1': [6 , 4],
                        'DM1': [14.5, 1],
                        'DS2': [0, 0],
                        'PROJET1': [0, 3],
                        'IE1': [7, 2],
                        'DS3': [9, 4],
                        'DS4':[15, 4]
                    }
            }
assert 10*moyenne("Dupont", resultats) == 145
assert 10*moyenne("Durand", resultats) == 94
assert 10*moyenne("Martin", resultats) == -10

# Autres tests
assert 10*moyenne("Durand", resultats_2) == 82


