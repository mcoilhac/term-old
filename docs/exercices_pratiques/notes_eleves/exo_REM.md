Cet exercice utilise ce que l'on appelle en anglais de l'"unpacking" que l'on pourrait traduire en français par "déballage"

👉 Nous l'avons souvent utilisé avec des tuples.

!!! example "Exemple"

	```python
	a, b, c = (1, 2, 3)  # Avec un tuple
	print(a)
	print(b)
	print(c)
	```
	

👉 Cette possibilité existe aussi avec des listes

!!! example "Exemple"

	```python
	a, b, c = [1, 2, 3]  # Avec une liste
	print(a)
	print(b)
	print(c)
	```
