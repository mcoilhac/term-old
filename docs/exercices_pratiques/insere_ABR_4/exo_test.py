# Tests

a1 = Noeud(5, None, None)
a1 = insere(a1, 2)
a1 = insere(a1, 3)
a1 = insere(a1, 7)
assert parcours(a1, []) == [2, 3, 5, 7]
a1 = insere(a1, 1)
a1 = insere(a1, 4)
a1 = insere(a1, 6)
a1 = insere(a1, 8)
assert parcours(a1, []) == [1, 2, 3, 4, 5, 6, 7, 8]


# Autres tests
a2 = a1
a2 = insere(a2, 10)
assert parcours(a2, []) == [1, 2, 3, 4, 5, 6, 7, 8, 10]
