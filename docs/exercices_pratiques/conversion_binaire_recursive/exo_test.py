# Tests
assert dec_to_bin(25) == '11001'
assert bin_to_dec('101010') == 42

# Autres tests
assert dec_to_bin(13) == "1101"
assert bin_to_dec("100") == 4
assert dec_to_bin(0) == "0"
assert bin_to_dec("0") == 0
