def parcours(adjacence, x, sommets_accessibles):
    if x not in sommets_accessibles: 
        sommets_accessibles.append(x)
        for y in adjacence[x]: 
            parcours(adjacence, y, sommets_accessibles) 

def accessibles(adjacence, x):
    sommets_accessibles = []
    parcours(adjacence, x, sommets_accessibles) 
    return sommets_accessibles

