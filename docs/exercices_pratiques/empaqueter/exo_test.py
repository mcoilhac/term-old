# Tests
assert empaqueter([1, 2, 3, 4, 5], 10) == 2
assert empaqueter([1, 2, 3, 4, 5], 5) == 4
assert empaqueter([7, 6, 3, 4, 8, 5, 9, 2], 11) == 5

# Autres tests
assert empaqueter([7, 6, 3, 4, 8, 5, 9, 2], 4) == 8
