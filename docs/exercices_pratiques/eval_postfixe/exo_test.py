# Tests
assert evaluation_postfixe([3, 2, '*', 5, '+']) == 11
assert evaluation_postfixe([2, 3, '+', 5, '*']) == 25
assert evaluation_postfixe([2]) == 2
assert evaluation_postfixe([2, 3, 4, '*', '*']) == 24

# Tests supplémentaires
assert evaluation_postfixe([3, 2, 5, '+', '+']) == 10
assert evaluation_postfixe([100, 3, '*', 5, '*']) == 1500
assert evaluation_postfixe([0]) == 0
assert evaluation_postfixe(list(range(1, 11))+['+']*9) == 55
assert evaluation_postfixe(list(range(1, 11))+['*']*9) == 3_628_800
