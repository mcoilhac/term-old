class Pile:
    """Classe définissant une structure de pile."""
    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon."""
        return self.contenu == []

    def empiler(self, element):
        """Place element au sommet de la pile"""
        self.contenu.append(element)

    def depiler(self):
        """
        Retire et renvoie l'élément placé au sommet de la pile,
        si la pile n'est pas vide.
        """
        if not self.est_vide():
            return self.contenu.pop()

def evaluation_postfixe(expression) :
    pile = Pile()
    for element in expression :
        if element != '+' ... element != '...':
            pile.empiler(...)
        else :
            if element == ...:
                resultat = pile.depiler() + ...
            else:
                resultat = ...
            pile.empiler(...)
    return ...

# Tests
assert evaluation_postfixe([3, 2, '*', 5, '+']) == 11
assert evaluation_postfixe([2, 3, '+', 5, '*']) == 25
assert evaluation_postfixe([2]) == 2
assert evaluation_postfixe([2, 3, 4, '*', '*']) == 24

