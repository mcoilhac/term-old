def binaire(a):
    '''convertit un nombre entier a en sa representation
    binaire sous forme de chaine de caractères.'''
    if a == 0:
        return ...
    bin_a = ...
    while ... :
        bin_a = ... + bin_a
        a = ...
    return bin_a

# Tests

assert binaire(0) == '0'
assert binaire(1) == '1'
assert binaire(16) == '10000'
assert binaire(77) == '1001101'


