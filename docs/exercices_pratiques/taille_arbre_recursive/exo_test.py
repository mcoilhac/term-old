# Tests

a = {'F':['B','G'], 'B':['A','D'], 'A':['',''], 'D':['C','E'], 
'C':['',''], 'E':['',''], 'G':['','I'], 'I':['','H'], 'H':['','']}

assert taille(a, 'F') == 9
assert taille(a, 'B') == 5
assert taille(a, 'I') == 2

# Autres tests

assert taille(a, 'E') == 1
assert taille(a, 'G') == 3
