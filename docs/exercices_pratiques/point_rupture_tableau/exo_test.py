# Tests

assert est_un_ordre([1, 6, 2, 8, 3, 7]) == False
assert est_un_ordre([5, 4, 3, 6, 7, 2, 1, 8, 9]) == True
assert nombre_points_rupture([5, 4, 3, 6, 7, 2, 1, 8, 9]) == 4
assert nombre_points_rupture([1, 2, 3, 4, 5]) == 0
assert nombre_points_rupture([1, 6, 2, 8, 3, 7, 4, 5]) == 7
assert nombre_points_rupture([2, 1, 3, 4]) == 2

# Autres tests

assert est_un_ordre([1, 4, 6, 2, 8, 3, 5, 7]) == True
assert est_un_ordre([1, 4, 6, 2, 8, 3, 5, 7, 10]) == False
assert nombre_points_rupture([1, 4, 6, 2, 8, 3, 5, 7]) == 8
