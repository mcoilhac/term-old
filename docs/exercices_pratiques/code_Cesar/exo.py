ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def position_alphabet(lettre) :
    return ord(lettre) - ord('A')

def cesar(message, decalage):
    resultat = ''
    for ... in message:
        if 'A' <= caractere and caractere <= 'Z':
            indice = (...) % 26
            resultat = resultat + ALPHABET[indice]
        else:
            resultat = ...
    return resultat

# Tests

assert cesar('BONJOUR A TOUS. VIVE LA MATIERE NSI !', 4) == 'FSRNSYV E XSYW. ZMZI PE QEXMIVI RWM !'
assert cesar('GTSOTZW F YTZX. ANAJ QF RFYNJWJ SXN !', -5) == 'BONJOUR A TOUS. VIVE LA MATIERE NSI !'
