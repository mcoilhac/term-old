!!! info "Grille de nombres"

    Une grille de nombre est aussi appelée **matrice**. Chaque élément de cette matrice se trouve à une certaine ligne, et à une certaine colonne.

    Il est habituel d'appeler `i` le numéro de la ligne, et `j` le numéro de la colonne.
    

!!! info "Listes en compréhension"

    Liste en compréhension pour des versions ultra compactes, mais néanmoins compréhensibles :

    ```python
    def negatif(image):
        return [[255 - image[i][j] for j in range(largeur(image))] for i in range(hauteur(image))]

    def binaire(image, seuil):
        return [[0 if image[i][j] < seuil else 1 for j in range(largeur(image))] for i in range(hauteur(image))]
    ```
