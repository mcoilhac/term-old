---
author: BNS 2022 n° 16 puis Gilles LASSUS et Mireille COILHAC
title: Positifs de pile
tags:
  - 7-pile
---

Cet exercice utilise des piles qui seront représentées en Python par des listes (type `list`).

On rappelle que l’expression `liste_1 = list(liste)` fait une copie de `liste `indépendante de `liste`, que
l’expression `x = liste.pop()` enlève le sommet de la pile `liste` et le place dans la variable `x` et,
enfin, que l’expression `liste.append(v)` place la valeur `v` au sommet de la pile `liste`.

Compléter le code Python de la fonction `positif` ci-dessous qui prend une pile de
nombres entiers en paramètre et qui renvoie la pile des entiers positifs dans le même
ordre, sans modifier la variable `pile`.

!!! example "Exemple"

    ```pycon
    >>> positif([-1, 0, 5, -3, 4, -6, 10, 9, -8])
    [0, 5, 4, 10, 9]
    >>> positif([-2])
    []
    ```

???+ question "Compléter ci-dessous"

    {{IDE('exo')}}
