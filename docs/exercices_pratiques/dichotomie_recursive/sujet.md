---
author: BNS 2022 n° 19 puis l'équipe e-nsi
title: Recherche dichotomique récursive
tags:
  - 5-dichotomie
  - 6-récursivité
  - important
---

# Rechercher un élément dans un tableau trié

L'objectif de cet exercice est d'écrire une fonction `indice` 

- qui prend en argument :
    - un tableau `valeurs` **rangé dans l'ordre croissant**
    - une valeur `cible`
- qui renvoie :
    - l'indice de `cible` dans le tableau s'il en fait partie
    - `None` sinon

La fonction `indice` utilisera une fonction `indice_recursive` qui sera récursive et qui prendra les mêmes arguments que `indice`, et en plus `debut` et `fin` qui désigneront les indices pour la recherche : de `debut` **inclus** à `fin` **inclus**.

> Le tableau `valeurs` pourra être rempli d'entiers ou rempli de chaines de caractères, sans aucun changement à procéder ; en effet ce sont des éléments comparables entre eux, ordre naturel pour les entiers, ordre lexicographique pour les chaines de caractères.

!!! example "Exemples"

    ```pycon
    >>> nombres = [2, 3, 5, 7, 11, 13, 17]
    >>> indice(nombres, 7)
    3
    >>> indice(nombres, 8) is None
    True
    ```

    ```pycon
    >>> fruits = ["abricot", "kiwi", "mangue", "poire", "pomme"]
    >>> fruits == sorted(fruits)  # le tableau est bien trié
    True
    >>> indice(fruits, "kiwi")
    1
    >>> indice(fruits, "cerise") is None
    True
    ```


???+ question "Compléter le code ci-dessous"

    {{ IDE('exo') }}
