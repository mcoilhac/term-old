def indice_recursive(valeurs, cible, debut, fin):
    if debut > fin:
        return None
    milieu = (debut + fin) // 2
    if valeurs[milieu] > cible:
        return indice_recursive(valeurs, cible, debut, milieu - 1)
    elif valeurs[milieu] < cible:
        return indice_recursive(valeurs, cible, milieu + 1, fin)
    else:
        return milieu


def indice(valeurs, cible):
    return indice_recursive(valeurs, cible, 0, len(valeurs) - 1)


