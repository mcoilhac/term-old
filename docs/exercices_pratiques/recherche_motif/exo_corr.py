def recherche_motif(motif, texte):
    solution = []
    i = 0
    while i <= len(texte) - len(motif):
        j = 0
        while j < len(motif) and motif[j] == texte[j+i]:
            j = j + 1
        if j == len(motif):
            solution.append(i)
        i = i + 1
    return solution

