# Tests

assert recherche_motif("ab", "") == []
assert recherche_motif("ab", "cdcdcdcd") == []
assert recherche_motif("ab", "abracadabra") == [0, 7]
assert recherche_motif("ab", "abracadabraab") == [0, 7, 11]

# Autres tests

assert recherche_motif("ra", "abracadabraab") == [2,9]
assert recherche_motif("za", "zaabracadabraabza") == [0, 15]
assert recherche_motif("za", "zaabracadabraabza") == [0, 15]
assert recherche_motif("cd", "cdcdcdcd") == [0, 2, 4, 6]
assert recherche_motif("cd", "cadcadcadcad") == []
