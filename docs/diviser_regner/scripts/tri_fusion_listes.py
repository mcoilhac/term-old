def tri_fusion(lst):
    """
    Précondition : lst est une liste
    Postcondition : la fonction renvoie une liste qui est la liste triée

    """
    ...


# Tests
assert tri_fusion([1, 2, 3, 4]) == [1, 2, 3, 4]
assert tri_fusion([4, 3, 2, 1]) == [1, 2, 3, 4]
assert tri_fusion([4, 2, 3, 1]) == [1, 2, 3, 4]
