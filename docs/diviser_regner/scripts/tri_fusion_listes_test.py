from random import shuffle
# Tests
assert tri_fusion([1, 2, 3, 4]) == [1, 2, 3, 4]
assert tri_fusion([4, 3, 2, 1]) == [1, 2, 3, 4]
assert tri_fusion([4, 2, 3, 1]) == [1, 2, 3, 4]

# Autres tests
assert tri_fusion([]) == []
ma_liste = [i for i in range(20)]
shuffle(ma_liste)
assert tri_fusion(ma_liste) == [i for i in range(20)]
