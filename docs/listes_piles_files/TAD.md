---
author: Mireille Coilhac
title: TAD - introduction
---

!!! info "TAD"

	👉 Un type abstrait de données (Abstract Data Type - ADT) = description d’un ensemble de données 

	🌵 Un TAD fait une abstraction de la structure de données (structure interne inconnue de l’extérieur) 

	👉Un TAD spécifie: 

	* Le type de données contenues 
	* Une description détaillée des opérations qui peuvent être effectuées sur les données 

	🌵Un TAD ne spécifie pas: 

	* La façon dont les données sont stockées 
	* Comment les méthodes sont implémentées

!!! info "Cette année"

	Nous allons ici étudier des structures de données linéaires : les listes, piles et files