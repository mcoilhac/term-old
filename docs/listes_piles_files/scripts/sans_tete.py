def enleve_tete(liste):
    """
    Cette fonction enlève la tête de la liste
    Précondition : liste est du type abstrait liste
    Postcondition : Cette fonction renvoie un type abstrait liste
    Exemple :

    >>> liste_1 = Vide()
    >>> liste_1 = Liste(1, liste_1)
    >>> liste_1 = Liste(2, liste_1)
    >>> liste_1 = Liste(3, liste_1)
    >>> enleve_tete(liste_1)
    [2, 1]

    """
    ...


# Tests
liste_1 = Vide()
liste_1 = Liste(1, liste_1)
liste_1 = Liste(2, liste_1)
liste_1 = Liste(3, liste_1)
print(enleve_tete(liste_1))
