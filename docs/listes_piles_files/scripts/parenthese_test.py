# Tests
assert verification("(3)*(2-5x)+3)") == False
assert verification("(3)*(2-5x)+3") == True

# Autres tests
assert verification("(3)*(2+6x)*(2-5x)+3)") == False
assert verification("(3)*(2-5(2+x)x)+3") == True

