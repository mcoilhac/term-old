def dico_lettres(mot):
    d = {}
    for i in range(len(mot)-1):
        d[mot[i]] = i
    return d

def BMH(texte, motif):
    dico = dico_lettres(motif)
    indices = []
    i = len(motif) - 1 # i sert à parcourir texte avec des sauts. On part du bout du motif.

    while i <= len(texte) - 1:
        k = 0
        while k < len(motif) and motif[len(motif) - 1 - k] == texte[i - k]: 
            k = k + 1
        if k == len(motif):   # On a trouvé le motif
            indices.append(i - len(motif) + 1)
            decalage = 1
            i = i + decalage  # On cherche un autre motif à droite
        else:
            if texte[i - k] in dico:
                decalage = len(motif) - 1 - dico[texte[i - k]]
                i = max(i - k  + decalage, i + 1)  # Il ne faut pas revenir en arrière
            else:
                decalage = len(motif)
                i = i - k + decalage  

    return indices

