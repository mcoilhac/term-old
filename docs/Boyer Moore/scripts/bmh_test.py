# Tests
assert BMH("stupid spring string", "string") == [14]
assert BMH("stupid spring string", "ing") == [10, 17]
assert BMH("stupid spring string", "ok") == []
assert dico_lettres("string") == {"s": 0, "t": 1, "r": 2, "i": 3, "n": 4}

# Autres tests
assert BMH("stupid spring string", "pi") == [3]
assert BMH("stupid spring string", "ri") == [9, 16]
