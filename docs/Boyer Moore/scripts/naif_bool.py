def recherche_naive_bool(texte, motif):
    '''
    renvoie un booléen indiquant la présence ou non de
    la chaîne motif dans la chaîne texte.
    '''
    trouve = False
    i = 0
    while i ... and  ...:
        k = 0
        while k < ... and texte[...] == motif[...]:
            k += 1
        if k == ...:
            trouve = ...
        i = ...

    return trouve

# Tests
assert recherche_naive_bool("une magnifique maison bleue", "maison") is True
assert recherche_naive_bool("une magnifique maison bleue", "nsi") is False
assert recherche_naive_bool("une magnifique maison bleue", "ma") is True

