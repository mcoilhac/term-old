# Tests
assert naif_envers("une magnifique maison bleue", "maison") == [15]
assert naif_envers("une magnifique maison bleue", "nsi") == []
assert naif_envers("une magnifique maison bleue", "ma") == [4, 15]

## Autres tests
assert naif_envers("une magnifique maison bleue", "za") == []
assert naif_envers('stupid spring string', 'string') == [14]
assert naif_envers('stupid spring string', 'ring') == [9, 16]