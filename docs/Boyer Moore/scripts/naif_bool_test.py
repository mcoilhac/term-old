# Tests
assert recherche_naive_bool("une magnifique maison bleue", "maison") is True
assert recherche_naive_bool("une magnifique maison bleue", "nsi") is False
assert recherche_naive_bool("une magnifique maison bleue", "ma") is True

## Autres tests
assert recherche_naive_bool("une magnifique maison bleue", "za") is False
assert recherche_naive_bool('stupid spring string', 'string') is True
assert recherche_naive_bool('stupid spring string', 'ring') is True
