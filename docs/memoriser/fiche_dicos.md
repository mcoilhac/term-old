---
author: Mireille Coilhac
title: Les dictionnaires - Bilan
---


!!! info "`mon_dico`"

    On appelle `mon_dico` le dictionnaire qui servira pour expliquer les syntaxes.

!!! abstract "Ajouts de couples (clé, valeur)"

    Il suffit de faire une nouvelle affectation : `mon_dico[cle] = nouvelle_valeur`


!!! abstract "Appartenance d'une clé dans un dictionnaire"

    * `cle in mon_dico` renvoie `True` si la clé `cle` existe dans `mon_dico` et `False` sinon.  
    * `cle not in mon_dico` renvoie `True` si la clé `cle` n'existe pas dans `mon_dico` et `False` sinon. 


!!! abstract "Accéder à une valeur"

    `mon_dico[cle]` renvoie la valeur associée à `cle` si elle est présente dans le dictionnaire, sinon une erreur `KeyError` se produit.
    

!!! abstract "Modifier une valeur"
 
    Il suffit de faire une nouvelle affectation : `mon_dico[cle] = nouvelle_valeur`


!!! abstract "Supprimer une valeur"

    Soit `valeur` la valeur associée à `cle`.

    * `del mon_dico[cle]` supprime le couple (`cle`, `valeur`) de `mon_dico`.
    * `mon_dico.pop(cle)` supprime le couple (`cle`, `valeur`) de `mon_dico` et renvoie la valeur correspondante.
    

!!! abstract "Ajouter un couple (`cle`, `valeur`)"

    Soit `valeur` la valeur que l'on souhaite associer à `cle`.  

    * Si la clé existe déjà `mon_dico[cle] = valeur` modifie la valeur associée,
    * sinon `mon_dico[cle] = valeur` ajoute la paire (`cle`, `valeur`)
    

!!! abstract "Longueur d'un dictionnaire"

    `len(mon_dico)` renvoie le nombre de couple (`cle`, `valeur`) du dictionnaire.


!!! abstract "Parcourir un dictionnaire"

    Le parcours avec la boucle `for element in mon_dico` permet de pacourir les clés de `mon_dico`


!!! abstract  "Utiliser les méthodes `keys`, `values` et `items`"

    On peut parcourir les vues créées par ces méthodes, de façon analogue à ce que l'on ferait avec d'autres séquences comme des listes :
    
    * `mon_dico.keys()` permet d'accéder à toutes les clés de `mon_dico`
    * `mon_dico.values()` permet d'accéder à toutes les valeurs de `mon_dico`
    * `mon_dico.items()` permet d'accéder à tous les couples (clé, valeur) de `mon_dico`
    

!!! abstract  "obtenir des listes de clés, valeurs, paires (clé, valeur)"

    On peut créer les listes de clés, de valeurs ou de couples (clé, valeur) :
    
    * `list(mon_dico.keys())` permet dobtenir une liste des clés de `mon_dico`
    * `list(mon_dico.values())` permet dobtenir une liste des valeurs de `mon_dico`
    * `list(mon_dico.items())` permet dobtenir une liste des tuples (clé, valeur) de `mon_dico`

