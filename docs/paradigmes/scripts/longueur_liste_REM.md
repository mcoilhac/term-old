Solution en écriture fonctionnelle : 

```python
longueur = lambda liste: 0 if est_vide(liste) else 1 + longueur(queue(liste))
```