# Tests

assert minimum(cons(4, cons(13, cons(1, cons(5, creer_liste()))))) == 1

# Autres tests

assert minimum(cons(1, cons(13, cons(1, cons(5, creer_liste()))))) == 1
assert minimum(cons(14, cons(13, cons(6, cons(5, creer_liste()))))) == 5
assert minimum(cons(4, cons(13, cons(2, cons(1, creer_liste()))))) == 1
