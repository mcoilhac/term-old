def somme_fonctions(f, g):
    return lambda x: f(x) + g(x)

def f1(x):
    return x**2

def f2(x):
    return 2*x

k = somme_fonctions(f1, f2)  # k est une fonction !

print(k(5))
