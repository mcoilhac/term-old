# Tests
assert hauteur(((None, 6, None), 15, None)) == 2
assert hauteur((None, 6, None)) == 1
assert hauteur(None) == 0

# Autres tests
assert hauteur((((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None)))) == 3
