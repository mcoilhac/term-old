Solution en écriture fonctionnelle : 

```python
minimum = lambda liste: tete(liste) if est_vide(queue(liste)) else min(tete(liste), minimum(queue(liste)))
```
