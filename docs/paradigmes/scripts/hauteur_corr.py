def hauteur(arbre):
    if est_vide(arbre):
        return 0
    else : 
        return 1 + max(hauteur(gauche(arbre)), hauteur(droite(arbre)))
        