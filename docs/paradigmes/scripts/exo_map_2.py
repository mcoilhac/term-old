nombres = (1, 2, 3, 4, 5)
resultat = map(lambda x: x ** 2, nombres)

print(resultat)  # Un objet itérable
print(type(resultat))
print(list(resultat))

