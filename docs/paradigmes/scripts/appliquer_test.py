# Tests
assert applique(lambda x: x*x, cons(4, cons(13, cons(1, cons(5, creer_liste()))))) == (16, (169, (1, (25, ()))))
assert est_vide(applique(lambda x: x*x, creer_liste()))

# Autres tests
assert applique(lambda x: x%2, cons(4, cons(13, cons(1, cons(6, creer_liste()))))) == (0, (1, (1, (0, ()))))
