---
authors: Mireille Coilhac
title: "Exercices sur le paradigme de programmation fonctionnelle"
---

## I. Rappels sur le paradigme fonctionnel

!!! info "Pas d'effets de bord"

    Avec le paradigme fonctionnel, tous les objets sont immuables et il n'y a pas de variables globales, pas d'effets de bords.

!!! info "Les affectations"

    Python permet d'écrire des scripts dans les trois paradigmes : impératif, programmation orientée objet, programmation fonctionelle. Nous allons expérimenter ici la programmation fonctionelle, donc s'interdire les affectation qui peuvent modifier l'état du programme, les utilisations d'objets mutables, les effets de bord.  

    Dans la plupart des langages fonctionnels il y a une commande qui permet de
    donner un nom à une expression avec une syntaxe comme `let EXPR be NOM in ...` Cela
    permet d’éviter que `NOM` reçoive une autre valeur dans le reste de la fonction, afin d’éviter
    les effets de bords. En Python, nous devrons "tricher" et utiliser des affectations en s’assurant qu’une variable ne recevra qu’une seule valeur lors de l’exécution de la fonction.

!!! info "Les boucles"

    En programmation fonctionelle, on n'utise pas de boucles `for`ni `while`. On utilise des fonctions récursives.

!!! info "Les fonctions sont des données"

    * Les fonctions peuvent être passées en paramètres d'autres fonctions.
    * Les fonctions peuvent renvoyer d'autres fonctions. C'est ce qu'on appelle l'ordre supérieur.


## II. Exercices sur la structure abstraite de Liste


???+ question "Implémentation des listes utilisée dans tous les exercices"

    Un exemple d'interface fonctionnelle pour le type abstrait Liste 

    La valeur d'une liste est un  `tuple` de `tuple` ce qui est une structure immuable en Python. 

    Exécuter le code ci-dessous (sinon les exercices ne fonctionneront pas). Il sera toujours présent (caché) dans les exercices de ce paragraphe

    {{ IDE('scripts/liste') }}

!!! example "Exemple d'utilisation"

    ```pycon
    >>> ma_liste = creer_liste()  # (1)
    >>> ma_liste
    ()
    >>> cons(1, ma_liste)
    (1, ())
    >>> ma_liste
    ()
    ```

    1. :warning: Ce sera la seule affectation de tous les exercices de cette page. Voir le paragraphe ci-dessus.

    !!! warning "Cliquer sur le + pour lire le commentaire"
    

???+ "Pas d'effet de bord"

    L'exemple précédent montre qu'on n'a pas d'effet de bord.


!!! warning "Contrainte"

    👉 Dans tous les exercices, il faudra utiliser des fonctions données ci-dessus dans l'implémentation de la structure abstraite liste.


### Exercice 0 : 

???+ question "Créer une liste"

    Ecrire l'instruction qui permet de créer la liste : `(4, (3, (2, (1, ()))))` sans utiliser aucune affectation.
    Faire l'affichage de la liste pour vérifier.

    {{ IDE('scripts/creer_liste_exemple') }}

??? success "Solution"

    ```python
    cons(4, cons(3, cons(2, cons(1, creer_liste()))))
    print(cons(4, cons(3, cons(2, cons(1, creer_liste())))))
    ```

### Exercice 1 :

???+ question "Longueur d'une liste"

    Compléter la fonction **récursive** `longueur` qui prend en paramètre une liste implémentée comme ci-dessus `liste`, 
    et renvoie la longueur de cette liste.

    {{ IDE('scripts/longueur_liste') }}


### Exercice 2 :

???+ question "Minimum d'une liste"

    Compléter la fonction **récursive** `minimum` qui prend en paramètre une liste **non vide** implémentée comme ci-dessus `liste`, 
    et renvoie le plus petit élément de cette liste.

    👉 il faudra utiliser la fonction `min` de python

    {{ IDE('scripts/minimum_liste') }}


### Exercice 3 :

???+ question "Une liste contient-elle un élément ?"

    Compléter la fonction **récursive** `contient` qui prend en paramètre une liste implémentée comme ci-dessus `liste`, et un élément `v`.
    Cette fonction doit renvoyer un booléen indiquant si `v` est dans liste.
   
    {{ IDE('scripts/contient_elem') }}


### Exercice 4 :

???+ question "Fonction d'ordre supérieur"

    Compléter `applique(f, liste)` qui renvoie une nouvelle liste correspondant aux images des éléments de `liste` par la fonction `f`.

    Exemple :    

    ```pycon 
    >>> applique(lambda x: x*x, cons(4, cons(13, cons(1, cons(5, creer_liste())))))
    (16, (169, (1, (25, ()))))
    ```
   
    {{ IDE('scripts/appliquer') }}

## III. Exercices sur la structure abstraite d'arbre binaire

On représentera dans tous les exercices qui suivent les arbres binaires ainsi :

* l'arbre vide est représenté par `#!py None`,
* un arbre non vide est représenté par un tuple `(sous-arbre gauche, valeur, sous-arbre droit)`.

Ainsi le tuple `((None, 6, None), 15, None)` représente l'arbre suivant :

<center>
```mermaid
graph TD
    R(15) --> A(6)
    R -.-x B( )
    A -.-x C( )
    A -.-x D( )
    style B display:none;
    style C display:none;
    style D display:none;
```
</center>


### Exercice 5 :

???+ question "Fonctions de base à réutiliser dans les autres exercices"

    Compléter les fonctions `est_vide`, `gauche`, `droite`, `racine` qui prennent en paramètre un arbre binaire 
    implémenté comme ci-dessus, et renvoient respectivement un booléen indiquant si l'arbre est vide, 
    le sous arbre gauche, le sous arbre droit, la racine de `arbre`

   
    {{ IDE('scripts/fcts_ab') }}


### Exercice 6 :

???+ question "Taille d'un arbre binaire"

    Compléter la fonction **récursive** `taille` qui prend en paramètre `arbre`, un arbre binaire implémenté comme ci-dessus, 
    et renvoie sa taille.
   
    {{ IDE('scripts/taille') }}


### Exercice 7 :

???+ question "Hauteur d'un arbre binaire"

    On convient que la hauteur d'un arbre ne contenant qu'un élément est 1.

    Compléter la fonction **récursive** `hauteur` qui prend en paramètre `arbre`, un arbre binaire implémenté comme ci-dessus, 
    et renvoie sa hauteur.

    👉 il faudra utiliser la fonction `max` de python
   
    {{ IDE('scripts/hauteur') }}


### Exercice 8 :

???+ question "Un arbre contient-il un élément ?"

    Compléter la fonction **récursive** `appartient` qui prend en paramètres `arbre`, un arbre binaire implémenté comme 
    ci-dessus, et un élément `valeur `. Cette fonction doit renvoyer un booléen indiquant si l'élément `valeur` est dans l'arbre.

    {{ IDE('scripts/appartenance') }}


### Exercice 9:

???+ question "Maximum d'une arbre"

    Compléter la fonction **récursive** `maximum` qui prend en paramètre un arbre binaire **non vide** implémentée comme ci-dessus `arbre`, 
    et renvoie le plus grand élément de cet arbre.

    👉 il faudra utiliser la fonction `max` de python. Vous pourrez utiliser la fonction `taille` vue plus haut.

    {{ IDE('scripts/maximum_arbre') }}


## Crédits 

Frédéric Junier, Romain Janvier 