# Tests
assert repeat(3,"bla") == "blablabla"
assert repeat(3,"") == ""

# Autres tests
assert repeat(0,"bla") == ""
assert repeat(4,"azertyuiop") == 4*"azertyuiop"