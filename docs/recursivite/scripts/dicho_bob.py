# Le code de Bob faux :

def recherche(lst, x):
    """
    Cette fonction renvoie True si x est dans lst, et False sinon
    Précondition : x : int, lst : list et triée en ordre croissant
    Postcondition : cette fonction renvoie du type booléen
    """

    if len(lst) == 0:
        return False
    m = len(lst) // 2
    if lst[m] == x:
        return True
    elif lst[m] < x:
        recherche(lst[m + 1:], x)
    else:
        recherche(lst[:m], x)

print(recherche([1, 2, 3, 4, 5, 6], 1))
print(recherche([1, 2, 3, 4, 5, 6], 6))
print(recherche([1, 2, 3, 4, 5, 6], 3))
print(recherche([1, 2, 3, 4, 5, 6], 8))
print(recherche([1, 2, 3, 4, 5, 6], 0))
print(recherche([1, 2, 3, 4, 5, 8], 7))

# Test

assert recherche([1, 2, 3, 4, 5, 6], 1) is True
assert recherche([1, 2, 3, 4, 5, 6], 0) is False