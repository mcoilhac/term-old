def u(n):
    """
    :param n: entier positif ou nul représentant le rang du terme de la suite
    :returns: Cette fonction renvoie la valeur de u(n)
    """
    if n == 0:
        return 5
    else:
        return 3*u(n - 1) + 2


