def triangle_bas(n):
    """Affiche un triangle tête en bas"""
    if n > 0:
        print('#' * n)
        triangle_bas(n - 1)

def triangle_haut(n):
    "Affiche un triangle tête en haut"
    if n > 0:
        triangle_haut(n - 1)
        print('#' * n)

triangle_haut(5)
triangle_bas(5)

