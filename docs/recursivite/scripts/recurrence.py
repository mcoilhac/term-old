def u(n):
    """
    :param n: entier positif ou nul représentant le rang du terme de la suite
    :returns: Cette fonction renvoie la valeur de u(n)
    """
    ...


# Tests
assert u(0) == 5
assert u(3) == 161
assert u(50) == 4307387926151115532621493





