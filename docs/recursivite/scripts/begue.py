def repeat(n: int, txt: str) -> str :
    """
    :param n: de type entier. Nombre de fois que l'on recopie la chaîne de caractères.
    :param txt: de type str. Chaine de caractères que l'on doit répéter.
    """
    ...


# Tests
assert repeat(3,"bla") == "blablabla"
assert repeat(3,"") == ""

