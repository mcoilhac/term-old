# Tests
assert longueur([]) == 0
assert longueur([1]) == 1
assert longueur([1, 2]) == 2
assert longueur([1, 2, 3]) == 3

# Autres tests

assert longueur([i for i in range(20)]) == 20
assert longueur([0 for i in range(30)]) == 30
