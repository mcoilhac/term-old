---
author: Mireille Coilhac
title: SGBD et SQL
---

## I. Système de gestion de base de données

Pour manipuler les données présentes dans une base de données (écrire, lire ou encore modifier), il est nécessaire d'utiliser un type de logiciel appelé "**s**ystème de **g**estion de **b**ase de **d**onnées" très souvent abrégé en SGBD.
Il existe une multitude de SGBD : gratuits/payants, libres/propriétaires...

### Les fonctions du SGBD

Les SGBD permettent de grandement simplifier la gestion des bases de données :

1. **Lecture, écriture, modification des informations** : les SGBD permettent de gérer les contenus dans une base de données

2. **Autorisations d'accès** : Il est en effet souvent nécessaire de contrôler les accès par exemple en permettant à l'utilisateur A de lire et d'écrire dans la base de données alors que l'utilisateur B aura uniquement la possibilité de lire les informations contenues dans cette même base de données.

3. **Sécuriser les bases de données** : les fichiers des bases de données sont stockés sur des disques durs, et peuvent subir des pannes. Il est souvent nécessaire que l'accès aux informations contenues dans une base de données soit maintenu, même en cas de panne matérielle. Les bases de données sont donc dupliquées sur plusieurs ordinateurs afin qu'en cas de panne d'un ordinateur A, un ordinateur B contenant une copie de la base de données présente dans A, puisse prendre le relais. Tout cela est très complexe à gérer, en effet toute modification de la base de données présente sur l'ordinateur A doit entrainer la même modification de la base de données présente sur l'ordinateur B. Cette synchronisation entre A et B doit se faire le plus rapidement possible, il est fondamental d'avoir des copies parfaitement identiques en permanence. C'est aussi les SGBD qui assurent la maintenance des différentes copies de la base de données.

4. **Accès concurrent** : plusieurs personnes peuvent avoir besoin d'accéder aux informations contenues dans une base de données en même temps. Cela peut parfois poser problème, notamment si les 2 personnes désirent modifier la même donnée au même moment (on parle d'accès concurrent). Ces problèmes d'accès concurrent sont aussi gérés par les SGBD. 

??? note pliée "Utilisation des SGBD"

    L'utilisation des SGBD explique en partie la supériorité de l'utilisation des BDD sur des solutions plus simples, mais aussi beaucoup plus limitées, comme les fichiers au format CSV.

## II. Le langage SQL

[Vidéo : cours SQL sur Lumni](https://www.lumni.fr/video/interrogation-d-une-base-de-donnees-relationnelle#containerType=serie&containerSlug=la-maison-lumni-lycee){ .md-button target="_blank" rel="noopener" }

### Premiers pas

???+ question "TD premiers pas"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `premiers_pas_sql_2023_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/premiers_pas_sql_2023_sujet.ipynb)
 
 
    😀 La correction est arrivée ...
    
    🌐 Fichier `premiers_pas_sql_2023_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/premiers_pas_sql_2023_corr.ipynb)

 <!--- 
 ⏳ La correction viendra bientôt ... 
😀 La correction est arrivée ...
    
🌐 Fichier `premiers_pas_sql_2023_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/premiers_pas_sql_2023_corr.ipynb)
 -->

### SQL : allons un peu plus loin


???+ question "TD/Cours : une seule table"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `une_table_sujet_2023.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/une_table_sujet_2023.ipynb)

    😀 La correction est arrivée ...

    🌐 Fichier `une_table_corr_2023_v2.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/une_table_corr_2023_v2.ipynb)

    

<!--- 

⏳ La correction viendra bientôt ... 

😀 La correction est arrivée ...

🌐 Fichier `une_table_corr_2023_v2.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/une_table_corr_2023_v2.ipynb)
-->

???+ question "TD/Cours : plusieurs tables - jointures"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `jointures_sujet_2023.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/jointures_sujet_2023.ipynb)

    😀 La correction est arrivée ...

    🌐 Fichier `jointures_corr_2023.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/jointures_corr_2023.ipynb)
    

    

<!--- 
⏳ La correction viendra bientôt ... 
😀 La correction est arrivée ...
🌐 Fichier `jointures_corr_2023.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/jointures_corr_2023.ipynb)
-->

### Bilan

!!! abstract "CREATE TABLE"

    ```sql
    CREATE TABLE LIVRES
    (id INT PRIMARY KEY, titre TEXT, auteur TEXT, ann_publi INT, note INT);
    ```

!!! abstract "INSERT INTO"

    ```sql
    INSERT INTO LIVRES
    (id, titre, auteur, ann_publi, note)
    VALUES
    (1, '1984', 'Orwell', 1949, 10);
    ```

!!! abstract "UPDATE... SET... WHERE..."

    ```sql
    UPDATE LIVRES
    SET auteur = 'R. Barjavel'
    WHERE auteur = 'Barjavel';
    ```

!!! abstract "DELETE FROM ... WHERE..."

    ```sql
    DELETE FROM LIVRES 
    WHERE auteur = 'Asimov';
    ```

!!! abstract "SELECT ... FROM ... WHERE..."

    ```sql
    SELECT titre, ann_publi
    FROM LIVRES
    WHERE auteur = 'Asimov';
    ```
    Autres comparaisons :

    ```sql
    SELECT titre, ann_publi
    FROM LIVRES
    WHERE 'Asimov' and ann_publi < 1954 
    WHERE auteur LIKE '%Asimov';
    ```

!!! abstract "ORDER BY"

    ```sql
    SELECT auteur, titre, ann_publi
    FROM LIVRES
    WHERE  ann_publi >= 1060
    ORDER BY ann_publi DESC;
    ```

!!! abstract "DISTINCT"

    ```sql
    SELECT DISTINCT auteur
    FROM LIVRES;
    ```

!!! abstract "SELECT ... FROM ... JOIN ON ..."

    ```sql
    SELECT *
    FROM LIVRES
    JOIN AUTEURS ON LIVRES.id_auteur = AUTEURS.id;
    ```

!!! abstract "SELECT ... FROM ... AS ... JOIN ON... AS ..."

    ```sql
    SELECT *
    FROM LIVRES AS l
    JOIN AUTEURS AS a ON l.id_auteur = a.id ;
    ```

#### Les agrégats

!!! abstract "COUNT(...)"

    ```sql
    SELECT COUNT(*)
    FROM LIVRES
    WHERE auteur LIKE "%Asimov";
    ```

!!! abstract "SUM(...)"

    ```sql
    SELECT SUM(note)
    FROM LIVRES
    WHERE auteur LIKE "%Asimov";
    ```

!!! abstract "AVG(...)"

    ```sql
    SELECT AVG(note)
    FROM LIVRES
    WHERE auteur LIKE "%Asimov";
    ```

!!! abstract "MIN(...) et MAX(...)"

    Les syntaxes pour MIN(...) et MAX(...) sont analogues à celles pour SUM et AVG

