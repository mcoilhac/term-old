---
author: Mireille Coilhac
title: Exercices de SQL
---

## TP 1

???+ question "Nos héros de bandes dessinées"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `heros_bd_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/heros_bd_sujet.ipynb)

	😀 La correction est arrivée ...

	Fichier `heros_bd_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/heros_bd_corr.ipynb)

<!--- 
⏳ La correction viendra bientôt ... 
😀 La correction est arrivée ...
🌐 Fichier `heros_bd_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/heros_bd_corr.ipynb)
-->

## TP 2

???+ question "Une médiathèque"

    Après avoir téléchargé les fichiers, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    Les deux fichiers suivants sont à télécharger et à mettre **dans le même dossier**.

    🌐 Fichier à télécharger : Fichier `Livres.db` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/Livres.db)

    🌐 TD à télécharger : Fichier `mediatheque_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mediatheque_sujet.ipynb)

	⏳ La correction viendra bientôt ...

<!--- 
⏳ La correction viendra bientôt ... 
😀 La correction est arrivée ...
🌐 Fichier `mediatheque_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mediatheque_corr.ipynb)
-->

## Gestion d'un réseau d'agences de location de voitures. 

<!--- 
[Lien de secours](http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/sql/){ .md-button target="_blank" rel="noopener" }
-->
!!! warning "Remarque"

    🌵 Dans ce TP, vous allez utiliser quelques syntaxes supplémentaires qui ne sont pas au programme de cette année.

	Les indications utiles vous seront données au fur et à mesure : n'oubliez pas de
	lire les explications ci-dessous avant de faire les exercices de la relation Agences, de la
	relation Vehicules, de la relation Locations, de la Gestion du Réseau.

* Aller sur [Colbert](https://colbert.bzh/start){ .md-button target="_blank" rel="noopener" }
* Cliquer sur $\fbox{Accès non authentifié mais dégradé}$
* Cliquer sur ![voiture](images/voiture.png){ width=5% }
* Cliquer successivement sur : 
	* La relation Agences
	* La relation Vehicules
	* La relation Locations
	* Gestion du Réseau



🚗🚓🚕

😀 Vous venez de finir brillamment vos études et confiant dans vos capacités, vous décidez de monter votre petite entreprise.

🤔Vous avez choisi de vous lancer dans la location de véhicules et en à peine 6 mois vous êtes déjà à la tête d'un réseau d'agences. Malheureusement, il apparaît que les rentrées financières ne décollent pas vraiment et il est temps d'analyser en détail votre historique des locations, le déplacement de vos véhicules et la synergie entre vos différentes agences.

👉 Après chaque question, cliquer sur le bouton "Valider", une nouvelle question apparaîtra pour vous aider dans cette étude.

😂 A vous de jouer : cliquer sur les liens suivants, dans l'ordre proposé.

La base de données [locations.db](a_telecharger/locations.db) contient les tables ```Agences```,```Locations```, ```Vehicules```.

![locations](images/diag_locations.png){ width=80% }

???+ question "La relation Agences"

    Répondez aux questions sur la relation Agence.

    Vous allez utiliser SELECT associé à des WHERE, WHERE LIKE et WHERE IN. Vous utiliserez également les fonctions COUNT et SUBSTR.

	🌵SUBSTR(char, m, n) : La fonction SUBSTR permet d’extraire de la chaine (char) n caractères depuis la position m
	Par exemple SUBSTR('Bonjour', 3, 2) renvoie "nj".  
	Attention, contrairement à ce que l'on fait en Python, le premier caractère est bien celui de position 1 (et pas de position 0)

	🌵L’opérateur logique IN dans SQL s’utilise avec la commande WHERE pour vérifier si une colonne est égale à une des valeurs comprise dans un ensemble (SET) de valeurs déterminées. C’est une méthode simple pour vérifier si une colonne est égale à une valeur OU une autre valeur OU une autre valeur et ainsi de suite, sans avoir à utiliser de multiple fois l’opérateur OR.

	```sql
	SELECT nom_colonne
	FROM table
	WHERE nom_colonne IN ( valeur1, valeur2, valeur3, ... );
	```

???+ question "La relation Véhicules"

	Répondez aux questions sur la relation Véhicules.

	Vous allez utiliser SELECT avec AS et ORDER BY. Vous utiliserez également les fonctions MAX, MIN, AVG, ROUND.

	🌵 On peut également réaliser des calculs :
	Par exemple le nombre moyen de kilomètres parcourus par moi s'obtient en calculant kilometrage/age

	🌵 La fonction ROUND() permet d’arrondir un résultat numérique.

???+ question "La relation Locations"

	Répondez aux questions sur la relation Locations.

    Vous définirez des jointures entre les trois relations avec des JOIN, AS et ON.


???+ question "La Gestion du réseau"

    Répondez aux questions sur la relation Véhicules.

    Vous utiliserez les commandes UPDATE, INSERT et DELETE.
	

## S'entraîner en autonomie

Travailler sur cet excellent site : [BDD Par Nicolas Revéret](https://nreveret.forge.apps.education.fr/exercices_bdd/){ .md-button target="_blank" rel="noopener" }


## Memento

✍ A noter Mémento PDF 🌐 Memento : Fichier `memento_SQL.pdf` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/memento_SQL.pdf)
