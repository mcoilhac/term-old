def deux_chiffes_ident(nbre:int)-> bool:
    """
    renvoie True si nbre a deux chiffres identiques, False sinon
    Exemples :
    >>> deux_chiffes_ident(121)
    True
    >>> deux_chiffes_ident(123)
    False


    """
    mot = str(nbre)
    if (mot[0] == mot[1] or mot[1] == mot[2]) or  mot[0] == mot[2]:
        return True
    else:
        return False

def somme_chiffres(nbre:int)->int:
    """
    renvoie la somme des chiffes de nbre
    Exemple :
    >>> somme_chiffres(123)
    6

    """
    mot = str(nbre)
    somme = 0
    for car in mot:
        somme = somme + int(car)
    return somme

possibles = [i for i in range(100, 300) if i % 2 ==0]
liste = [nbre for nbre in possibles if (deux_chiffes_ident(nbre) == True and somme_chiffres(nbre) == 7)]




