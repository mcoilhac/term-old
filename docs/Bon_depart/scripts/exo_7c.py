def effectifs(donnees):
    dico_effect = dict()
    for v in donnees:
        if v in dico_effect:
            dico_effect[v] = ...
        else:
            dico_effect[v] = ...
    return dico_effect

# tests
assert effectifs([4, 1, 2, 4, 2, 2, 6]) == {4: 2, 1: 1, 2: 3, 6: 1}
assert effectifs(["chien", "chat", "chien", "chien", "poisson", "chat"]) == {'chien': 3, 'chat': 2, 'poisson': 1}
