# tests
assert compter('b', 'bulle') == 1
assert compter('l', 'bulle') == 2
assert compter('v', 'bulle') == 0
# tests secrets
assert compter('b', '') == 0
assert compter('B', 'bulle') == 0
assert compter('e', 'bulle') == 1
assert compter('a', 'maman') == 2
assert compter('x', 'x'*1000) == 1000
