def indiscernables(nombre1, nombre2):
    return abs(nombre1 - nombre2) < 1e-15

def moyenne(valeurs):
    ...

# tests
assert indiscernables(moyenne([5]), 5.0)
assert indiscernables(moyenne([5, 15, 8]), 9.333333333333334)
assert indiscernables(moyenne([5, 15, 10]), 10.0)
