# tests
assert position(7, [5, -1, 7, 4, 6, 4, 2]) == 2
assert position(4, [5, -1, 7, 4, 6, 4, 2]) == 3
assert position(0, [5, -1, 7, 4, 6, 4, 2]) == None
# tests secrets
assert position(0, []) == None
assert position(0, [0]) == 0
assert position(0, [0, 0]) == 0
assert position(0, [1, 0, 2]) == 1
assert position(0, [1, 2, 0]) == 2
