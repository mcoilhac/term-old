def compter(symbole, texte):
    ...
    for s in texte:
        if ...:
            ...
    return ...

# tests
assert compter('b', 'bulle') == 1
assert compter('l', 'bulle') == 2
assert compter('v', 'bulle') == 0
