---
author: Mireille Coilhac
title: Algorithmes - Les indispensables
---

!!! warning "Les indispensables"

    Les indispensables sont ... indispensables  😂

## I. Recherche dichotomique

!!! info "Mon info"

    La recherche dichotomique permet de rechercher un entier dans une liste **triée**, ainsi que sa **position**. 

??? note pliée "Quel est le principe de la dichotomie ? réfléchir avant de cliquer 😊 "

	Le principe de la recherche dichotomique d'un entier `v` dans une liste **triée** de n éléments est le suivant :

    * Si `v` est égal à l'élément se trouvant au milieu de la liste liste[milieu] , l'entier `v` est trouvé, et on renvoie sa position.
    * Sinon si `v` <  `liste[milieu]`, on recommence la recherche dans la première moitié de la liste : `liste[0 -> milieu-1]`
    * Sinon on recommence la recherche dans la seconde moitié de la liste : `liste[milieu + 1 -> fin]`


??? question "Exercice 1 : Appartenance par dichotomie"

    Compléter la fonction `dichotomie` qui prend en paramètre une liste Python `nombres` et un valeur `cible`. Cette fonction renvoie `True` si `cible` est dans `nombres` et `False` sinon.

    === "Version sans code à trous"
        {{ IDE('scripts/dicho_bool_vide') }}

    === "Version avec code à trous"
        {{ IDE('scripts/dicho_bool_trous') }}

??? question "Exercice 2 : Appartenance et indice par dichotomie"

    Compléter la fonction `indice` qui prend en paramètre une liste Python `tableau` et une valeur `cible`. Cette fonction renvoie l'indice de `cible` dans `tableau` si `cible` est dans `tableau` et `None` sinon.

    === "Version sans code à trous"
        {{ IDE('scripts/dicho_indice_vide') }}

    === "Version avec code à trous"
        {{ IDE('scripts/dicho_indice_trous') }}


??? note pliée "Quelle est la complexité d'un algorithme de dichotomie ? réfléchir avant de cliquer 😊"

    💚 A retenir : L’algorithme de dichotomie a une **complexité logarithmique** de l'ordre de  $\log(n)$ pour une liste de taille $n$.


## II. Le tri par sélection

??? note pliée "Quel est le principe du tri par sélection ? réfléchir avant de cliquer 😊"

    Pour un tableau `tab` de taille `n`

    ```
    pour i allant de 0 à n-2
    pos ← position du minimum dans tab à partir du rang 
    si pos ≠  i :
        échanger tab[i] et tab[pos]
    ```

???+ question "La fonction `#!py tri_selection`"

    Compléter la fonction `#!py tri_selection` prenant en argument un `#!py tableau` et le triant **en place** à l'aide du tri par sélection.

    === "Version sans code à trous"
        {{ IDE('scripts/selection_vide') }}

    === "Version avec code à trous"
        {{ IDE('scripts/selection_trous') }}

???+ question "Utiliser La fonction `#!py tri_selection`"

    Exécuter le script suivant :

    {{ IDE('scripts/selection_bord') }}

    La liste de départ a été **modifiée** ...

    C'est ce qu'on appelle un **effet de bord**. 
    
    La fonction a modifié **"en place"** la liste.

!!! abstract "Résumé"

    * Le plus souvent, on écrit une procédure (pas de `return`) pour le tri par selection.
    * C'est un tri "en place" qui modifie la liste elle-même.
    * 👉 Vous devez mémoriser cette procédure.


??? note pliée "Quel est la complexité d'un algorithme de tri par selection ? réfléchir avant de cliquer 😊"

    💚 A retenir : L’algorithme du tri par selection a une complexité quadratique de l'ordre de $n^2$ pour une liste de taille $n$.



## III. Le tri par insertion

??? note pliée "Quel est le principe du tri par insertion ? réfléchir avant de cliquer 😊"

    Pour un tableau `tab` de taille `n`

    ```python
    pour i allant de 1 à n-1
        clef ← tab[i]
        insérer la clef au bon endroit dans tab 
    ```

???+ question "La fonction `#!py tri_insertion`"

    Compléter la fonction `#!py tri_insertion` prenant en argument un `#!py tableau` et le triant **en place** à l'aide du tri par insertion.

    === "Version sans code à trous"
        {{ IDE('scripts/insertion_vide') }}

    === "Version avec code à trous"
        {{ IDE('scripts/insertion_trous') }}

???+ question "Utiliser La fonction `#!py tri_insertion`"

    Exécuter le script suivant :

    {{ IDE('scripts/insertion_bord') }}

    La liste de départ a été **modifiée** ...

    C'est ce qu'on appelle un **effet de bord**. 
    
    La fonction a modifié **"en place"** la liste.

!!! abstract "Résumé"

    * Le plus souvent, on écrit une procédure (pas de `return`) pour le tri par insertion.
    * C'est un tri "en place" qui modifie la liste elle-même.
    * 👉 Vous devez mémoriser cette procédure.


??? note pliée "Quel est la complexité d'un algorithme de tri par insertion ? réfléchir avant de cliquer 😊"

    💚 A retenir : L’algorithme du tri par insertion a une complexité quadratique de l'ordre de $n^2$ pour une liste de taille $n$.

